/*let userUserName;
let userPassword;
let userRole;

function getUserInfo(){
	userUserName = prompt("Enter your username");
	userPassword = prompt("Enter your password");
	userRole = prompt("Enter your role");

	if(userUserName === null || userUserName === ""){
		alert ("Username field must not be empty");
	}
	if(userPassword === null || userPassword === ""){
		alert ("Password field must not be empty");
	}
	if(userRole === null || userRole === ""){
		alert ("Role field must not be empty");
	}
	else {
		switch (userRole){
			case "admin":
			alert ("Welcome back to the class portal, admin!");
			break;

			case "teacher":
			alert ("Thank you for logging in, teacher!");
			break;

			case "student":
			alert ("Welcome to the class portal, student!");
			break;

			default:
			alert ("Role out of range.");
			break;
		}
	}
	
}

let userInfo = getUserInfo();
console.log(userInfo);
*/

function getAverage (num1, num2, num3, num4){
	let avg = ((num1+num2+num3+num4)/4);
		avg = Math.round(avg);

		if (avg <= 74){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is F");
		}
		else if (avg <=79){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is D");
		}
		else if (avg <=84){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is C");
		}
		else if (avg <=89){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is B");
		}
		else if (avg <=95){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is A");
		}
		else if (avg <=100){
			console.log ("Hello, student, your average is " + avg + "." + " The letter equivalent is A+");
		}
}

let checkAverage = getAverage(90,89,77,85);
console.log (checkAverage);

checkAverage = getAverage(65,88,74,90);
console.log (checkAverage);

checkAverage = getAverage(99,87,75,82);
console.log (checkAverage);

checkAverage = getAverage(73,70,77,74);
console.log (checkAverage);

